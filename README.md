Evolutionary algorithm
======================

This is a simple implementation of the evolutionary algorithm used to solve the
knapsack problem. This implementation was made just for educational purposes.

Dependencies
============

I use `pip` to manage dependencies, but they are just `numpy`.

```console
pip install -r requirements.txt
```

You need `Python3` to run it.

Running
=======

Just run python:

```console
python knapsack.py
```

About the algorithm
===================

This implementation just run a tournament selection. I'm not sure if I will
implement other selection methods. Feel free to fork it and do it yourself.

The initialization is done randomly.

Read more about this type of algorithms on it's [Wikipedia
page](https://en.wikipedia.org/wiki/Evolutionary_algorithm).
