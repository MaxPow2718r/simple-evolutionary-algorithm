# The objective is to find the best convination that maximizes the total value
# carried while keeping the weight within the knapsack capacity.
import random
import numpy as np
import time

# This items contains the weight and value
items = [(2, 10), (3, 15), (5, 25), (7, 30)]
CAPACITY = 10


def fitness_function(individual, item, capacity):
    """
    This function calculates the total value of a given individual's items based on a
    given item list and capacity.

    Parameters:
        individual (list): A list of booleans representing the items selected by the individual.
        item (list): A list of tuples containing the value and weight of each item.
        capacity (int): The maximum weight capacity of the individual.

    Returns:
        total_value (int): The total value of the individual's items.
    """
    total_weight = 0
    total_value = 0

    for i, selected in enumerate(individual):
        if selected:
            total_weight += item[i][0]
            total_value += item[i][1]

    if total_weight > capacity:
        total_value = 0

    return total_value


def init_population(population_size, individual_size):
    """
    This function initializes a population of individuals with a given population size and individual size.

    Parameters:
        population_size (int): The size of the population to be initialized.
        individual_size (int): The size of each individual in the population.

    Returns:
        population (list): A list of individuals with the given population size and individual size.
    """
    population = []

    for _ in range(population_size):
        individual = np.random.randint(2, size=individual_size)
        population.append(individual)

    return population


def tournament_selection(population, items, tournament_size, capacity):
    """
    This function performs tournament selection on a given population.

    Parameters:
        population (list): A list of individuals from which to select.
        items (list): A list of items to be used in the fitness function.
        tournament_size (int): The number of individuals to select from the population.
        capacity (int): The capacity of the knapsack.

    Returns:
        selected (list): A list of individuals selected from the population.
    """
    selected = []

    for _ in range(len(population)):
        tournament = random.choices(population, k=tournament_size)

        tournament_fitness = [
            fitness_function(individual, items, capacity) for individual in tournament
        ]
        selected.append(tournament[tournament_fitness.index(max(tournament_fitness))])

    return selected


def crossover(parent1, parent2):
    """
    This function takes two parent arrays as input and returns two child arrays as
    output. It randomly selects two points in the parent arrays and then combines the
    elements of the two parents in between those points to create the two children.
    """
    points1 = random.randint(0, len(parent1) - 2)
    points2 = random.randint(points1 + 1, len(parent1) - 1)
    child1 = np.concatenate(
        (parent1[:points1], parent2[points1:points2], parent1[points2:])
    )
    child2 = np.concatenate(
        (parent2[:points1], parent1[points1:points2], parent2[points2:])
    )

    return child1, child2


def mutation(individual, mutation_rate):
    """
    This function takes in an individual (list) and a mutation rate (float) and returns the individual with a bit flip mutation applied.

    Parameters:
        individual (list): The individual to be mutated.
        mutation_rate (float): The rate at which the mutation should occur.

    Returns:
        individual (list): The mutated individual.
    """
    for i in range(len(individual)):
        if random.random() < mutation_rate:
            # bit flip
            individual[i] = 1 - individual[i]

    return individual


def evolutionary_algorithm(
    population_size,
    individual_size,
    generations,
    tournament_size,
    mutation_rate,
    items,
    capacity,
):
    """
    Runs an evolutionary algorithm to solve the knapsack problem.

    Parameters:
        population_size (int): The size of the population.
        individual_size (int): The size of each individual in the population.
        generations (int): The number of generations to run the algorithm for.
        tournament_size (int): The size of the tournament for tournament selection.
        mutation_rate (float): The rate at which mutation should occur.
        items (list): A list of tuples containing the weight and value of each item.
        capacity (int): The capacity of the knapsack.

    Returns:
        best_individual (list): The best individual found by the algorithm.
        best_fitness (int): The fitness of the best individual.
    """
    population = init_population(population_size, individual_size)

    for _ in range(generations):
        selected = tournament_selection(population, items, tournament_size, capacity)

        new_population = []
        for i in range(0, population_size, 2):
            parent1 = selected[i]
            parent2 = selected[i + 1]

            child1, child2 = crossover(parent1, parent2)

            child1 = mutation(child1, mutation_rate)
            child2 = mutation(child2, mutation_rate)

            new_population.append(child1)
            new_population.append(child2)

        population = new_population

        best_individual = max(
            population, key=lambda ind: fitness_function(ind, items, capacity)
        )
        best_fitness = fitness_function(best_individual, items, capacity)

        return best_individual, best_fitness


if __name__ == "__main__":
    population_size = 10
    individual_size = len(items)
    generations = 1000000
    tournament_size = 4
    mutation_rate = 0.3
    items = items
    capacity = CAPACITY

    start = time.time()
    best_individual, best_fitness = evolutionary_algorithm(
        population_size,
        individual_size,
        generations,
        tournament_size,
        mutation_rate,
        items,
        capacity,
    )
    end = time.time()
    time_taken = end - start

    print(best_individual, best_fitness, "time taken = ", time_taken)
